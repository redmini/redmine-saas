# Redmine SAAS

Redmine SAAS is a fork of redmine tuned for easy deploy to heroku.

## Setting up

```bash
heroku create 
heroku config:set RAILS_ENV=production
heroku config:set SECRET_KEY_BASE=`head /dev/urandom | md5sum`
git push heroku master
heroku run rake db:migrate
heroku run rake redmine:load_default_data
```